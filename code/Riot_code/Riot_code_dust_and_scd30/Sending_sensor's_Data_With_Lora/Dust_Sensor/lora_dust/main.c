

#include <stdio.h>

#include "cayenne_lpp.h"
/*
 * Copyright (C) 2018 Inria
 *
 * This file is subject to the terms and conditions of the GNU Lesser
 * General Public License v2.1. See the file LICENSE in the top level
 * directory for more details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Example demonstrating the use of LoRaWAN with RIOT
 *
 * @author      Alexandre Abadie <alexandre.abadie@inria.fr>
 *
 * @}
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
// Replace REPLACE_ME with TTN_FP_EU868 or TTN_FP_US915

#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#include "xtimer.h"
#include "timex.h"
#include "periph/gpio.h"
#include "msg.h"
#include "thread.h"
#include "fmt.h"

#define CHANNEL_CONCENTRATION 0
#define CHANNEL_RATIO 1
#define CHANNEL_LOWPULS 2

#if IS_USED(MODULE_PERIPH_RTC)
#include "periph/rtc.h"
#else
#include "timex.h"
#include "ztimer.h"
#endif

#include "net/loramac.h"
#include "semtech_loramac.h"

#if IS_USED(MODULE_SX127X)
#include "sx127x.h"
#include "sx127x_netdev.h"
#include "sx127x_params.h"
#endif

#if IS_USED(MODULE_SX126X)
#include "sx126x.h"
#include "sx126x_netdev.h"
#include "sx126x_params.h"
#endif

/* Messages are sent every 20s to respect the duty cycle on each channel */
#define PERIOD_S (5U)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define SENDER_PRIO (THREAD_PRIORITY_MAIN - 1)

static semtech_loramac_t loramac;
#if IS_USED(MODULE_SX127X)
static sx127x_t sx127x;
#endif
#if IS_USED(MODULE_SX126X)
static sx126x_t sx126x;
#endif


static uint8_t deveui[LORAMAC_DEVEUI_LEN];
static uint8_t appeui[LORAMAC_APPEUI_LEN];
static uint8_t appkey[LORAMAC_APPKEY_LEN];

unsigned long duration;
time_t starttime;
time_t currenttime;
unsigned long sampletime_s = 3;
int pulse_time_out = 20;
unsigned long lowpulseoccupancy = 0;
float ratio = 0;
float concentration = 0;
gpio_t pin;

unsigned long pulseIn(void)
{
    int high = 0;
    time_t start_timer = 0;
    time_t end_timer = 0;
    time_t start = 0;
    time_t end = 0;
    time(&start_timer);
    while (high == 0)
    {

        if (gpio_read(pin) > 0)
        {

            high = 1;
            time(&start);

            break;
        }
        time(&end_timer);
        if (end_timer - start_timer > pulse_time_out)
            return 0;
    }

    if (high == 1)
    {

        while (1)
        {
            if (gpio_read(pin) == 0)
            {
                high = 0;

                time(&end);

                break;
            }
            time(&end_timer);
            if (end_timer - start_timer > pulse_time_out)
                return 0;
        }
    }
    unsigned long elapsed = end - start;

    return elapsed;
}

/* Function to calculate x raised to the power y */
int pow(int x, unsigned int y)
{
    if (y == 0)
        return 1;
    else if (y % 2 == 0)
        return pow(x, y / 2) * pow(x, y / 2);
    else
        return x * pow(x, y / 2) * pow(x, y / 2);
}

void setup(void)
{
    // pin = GPIO_PIN(2,12);
    pin = GPIO_PIN(1, 10);
    gpio_init(pin, GPIO_IN);

    time(&starttime); // get the current time;
}
static cayenne_lpp_t lpp = {0};
float result[3]={0};
void getData(void)
{

    duration = pulseIn();
    duration = duration * 1000000;
    lowpulseoccupancy = lowpulseoccupancy + duration;
    time(&currenttime);
    unsigned long elapsed = currenttime - starttime;

    if (elapsed >= sampletime_s) // if the sampel time == 30s
    {
        long int sampletime_ms = sampletime_s * 1000;
        ratio = lowpulseoccupancy / (sampletime_ms * 10.0);                             // Integer percentage 0=>100
        concentration = 1.1 * pow(ratio, 3) - 3.8 * pow(ratio, 2) + 520 * ratio + 0.62; // using spec sheet curve
        printf("lowpulseoccupancy = %ld , ratio = %f, concentration = %f\r\n", lowpulseoccupancy, ratio, concentration);

        lowpulseoccupancy = 0;
        time(&starttime);
    }
   result[0]=concentration;
   result[1]=ratio;
   result[2]=lowpulseoccupancy;
    
   
}



static void _send_message(void)
{
    cayenne_lpp_reset(&lpp);

    getData();
    if(result[0]>0.63){
    cayenne_lpp_add_analog_output(&lpp, CHANNEL_CONCENTRATION, result[0]);
    cayenne_lpp_add_analog_output(&lpp, CHANNEL_RATIO, result[1]);
    cayenne_lpp_add_analog_output(&lpp, CHANNEL_LOWPULS, result[2]);

    printf("Sending a message\n");
    /* Try to send the message */
    uint8_t ret = semtech_loramac_send(&loramac, lpp.buffer, lpp.cursor);

    if (ret != SEMTECH_LORAMAC_TX_DONE)
    {
        printf("Cannot send message '%s', ret code: %d\n", lpp.buffer, ret);
        return;
    }
    }
}


int main(void)
{
    puts("LoRaWAN Class A low-power application");
    puts("=====================================");

    /* Convert identifiers and application key */
    fmt_hex_bytes(deveui, CONFIG_LORAMAC_DEV_EUI_DEFAULT);
    fmt_hex_bytes(appeui, CONFIG_LORAMAC_APP_EUI_DEFAULT);
    fmt_hex_bytes(appkey, CONFIG_LORAMAC_APP_KEY_DEFAULT);

    /* Initialize the radio driver */
#if IS_USED(MODULE_SX127X)
    sx127x_setup(&sx127x, &sx127x_params[0], 0);
    loramac.netdev = &sx127x.netdev;
    loramac.netdev->driver = &sx127x_driver;
#endif

#if IS_USED(MODULE_SX126X)
    sx126x_setup(&sx126x, &sx126x_params[0], 0);
    loramac.netdev = &sx126x.netdev;
    loramac.netdev->driver = &sx126x_driver;
#endif

    /* Initialize the loramac stack */
    semtech_loramac_init(&loramac);
    semtech_loramac_set_deveui(&loramac, deveui);
    semtech_loramac_set_appeui(&loramac, appeui);
    semtech_loramac_set_appkey(&loramac, appkey);

    /* Use a fast datarate, e.g. BW125/SF7 in EU868 */
    semtech_loramac_set_dr(&loramac, LORAMAC_DR_5);

    /* Start the Over-The-Air Activation (OTAA) procedure to retrieve the
     * generated device address and to get the network and application session
     * keys.
     */
    puts("Starting join procedure");
    if (semtech_loramac_join(&loramac, LORAMAC_JOIN_OTAA) != SEMTECH_LORAMAC_JOIN_SUCCEEDED)
    {
        puts("Join procedure failed");
        return 1;
    }
    puts("Join procedure succeeded");
    setup();
    time_t start_timer = 0;
    time_t end_timer = 0;
    time(&start_timer);
    getData();
    while (1)
    {
        time(&end_timer);
        if (end_timer-start_timer>5)
        {
            _send_message();
            time(&start_timer);
        }
        
        
       
       
    }
    
    return 0;
}
