

#include <stdio.h>

#include "cayenne_lpp.h"
/*
 * Copyright (C) 2018 Inria
 *
 * This file is subject to the terms and conditions of the GNU Lesser
 * General Public License v2.1. See the file LICENSE in the top level
 * directory for more details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Example demonstrating the use of LoRaWAN with RIOT
 *
 * @author      Alexandre Abadie <alexandre.abadie@inria.fr>
 *
 * @}
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
// Replace REPLACE_ME with TTN_FP_EU868 or TTN_FP_US915

#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#include "xtimer.h"
#include "timex.h"
#include "periph/gpio.h"
#include "msg.h"
#include "thread.h"
#include "fmt.h"

#define CHANNEL_PPM_CO2 0
#define CHANNEL_PPM_temp 1
#define CHANNEL_PPM_humidity 2
#define MEASUREMENT_INTERVAL_SECS (2)
#if IS_USED(MODULE_PERIPH_RTC)
#include "periph/rtc.h"
#else
#include "timex.h"
#include "ztimer.h"
#endif

#include "scd30.h"
#include "scd30_params.h"
#include "scd30_internal.h"
#include "net/loramac.h"
#include "semtech_loramac.h"

#if IS_USED(MODULE_SX127X)
#include "sx127x.h"
#include "sx127x_netdev.h"
#include "sx127x_params.h"
#endif

#if IS_USED(MODULE_SX126X)
#include "sx126x.h"
#include "sx126x_netdev.h"
#include "sx126x_params.h"
#endif

/* Messages are sent every 20s to respect the duty cycle on each channel */
#define PERIOD_S (5U)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define SENDER_PRIO (THREAD_PRIORITY_MAIN - 1)

static semtech_loramac_t loramac;
#if IS_USED(MODULE_SX127X)
static sx127x_t sx127x;
#endif
#if IS_USED(MODULE_SX126X)
static sx126x_t sx126x;
#endif


static uint8_t deveui[LORAMAC_DEVEUI_LEN];
static uint8_t appeui[LORAMAC_APPEUI_LEN];
static uint8_t appkey[LORAMAC_APPKEY_LEN];


scd30_t scd30_dev;
scd30_params_t params = SCD30_PARAMS;
scd30_measurement_t result;


 uint16_t pressure_compensation = SCD30_DEF_PRESSURE;
    uint16_t value = 0;
    uint16_t interval = MEASUREMENT_INTERVAL_SECS;


void setup_scd30(void){
    scd30_init(&scd30_dev, &params);

    scd30_set_param(&scd30_dev, SCD30_INTERVAL, MEASUREMENT_INTERVAL_SECS);
    scd30_set_param(&scd30_dev, SCD30_START, pressure_compensation);

    scd30_get_param(&scd30_dev, SCD30_INTERVAL, &value);
    printf("[test][dev-%d] Interval: %u s\n", 0, value);
    scd30_get_param(&scd30_dev, SCD30_T_OFFSET, &value);
    printf("[test][dev-%d] Temperature Offset: %u.%02u C\n", 0, value / 100u,
           value % 100u);
    scd30_get_param(&scd30_dev, SCD30_A_OFFSET, &value);
    printf("[test][dev-%d] Altitude Compensation: %u m\n", 0, value);
    scd30_get_param(&scd30_dev, SCD30_ASC, &value);
    printf("[test][dev-%d] ASC: %u\n", 0, value);
    scd30_get_param(&scd30_dev, SCD30_FRC, &value);
    printf("[test][dev-%d] FRC: %u ppm\n", 0, value);
}


void get_mesurement(void){
    scd30_read_periodic(&scd30_dev, &result);
    printf(
            " Continuous measurements co2: %.02fppm,"
            " temp: %.02f°C, hum: %.02f%%. \n", result.co2_concentration,
            result.temperature, result.relative_humidity);
}

static cayenne_lpp_t lpp = {0};




static void _send_message(void)
{
    cayenne_lpp_reset(&lpp);

    get_mesurement();
    cayenne_lpp_add_analog_output(&lpp, CHANNEL_PPM_CO2, (float) result.co2_concentration);
    cayenne_lpp_add_analog_output(&lpp, CHANNEL_PPM_temp, (float) result.temperature);
    cayenne_lpp_add_analog_output(&lpp, CHANNEL_PPM_humidity, (float) result.relative_humidity);

    printf("Sending a message\n");
    /* Try to send the message */
    uint8_t ret = semtech_loramac_send(&loramac, lpp.buffer, lpp.cursor);

    if (ret != SEMTECH_LORAMAC_TX_DONE)
    {
        printf("Cannot send message '%s', ret code: %d\n", lpp.buffer, ret);
        return;
    }
}


int main(void)
{
    puts("LoRaWAN Class A low-power application");
    puts("=====================================");

    /* Convert identifiers and application key */
    fmt_hex_bytes(deveui, CONFIG_LORAMAC_DEV_EUI_DEFAULT);
    fmt_hex_bytes(appeui, CONFIG_LORAMAC_APP_EUI_DEFAULT);
    fmt_hex_bytes(appkey, CONFIG_LORAMAC_APP_KEY_DEFAULT);

    /* Initialize the radio driver */
#if IS_USED(MODULE_SX127X)
    sx127x_setup(&sx127x, &sx127x_params[0], 0);
    loramac.netdev = &sx127x.netdev;
    loramac.netdev->driver = &sx127x_driver;
#endif

#if IS_USED(MODULE_SX126X)
    sx126x_setup(&sx126x, &sx126x_params[0], 0);
    loramac.netdev = &sx126x.netdev;
    loramac.netdev->driver = &sx126x_driver;
#endif

    /* Initialize the loramac stack */
    semtech_loramac_init(&loramac);
    semtech_loramac_set_deveui(&loramac, deveui);
    semtech_loramac_set_appeui(&loramac, appeui);
    semtech_loramac_set_appkey(&loramac, appkey);

    /* Use a fast datarate, e.g. BW125/SF7 in EU868 */
    semtech_loramac_set_dr(&loramac, LORAMAC_DR_5);

    /* Start the Over-The-Air Activation (OTAA) procedure to retrieve the
     * generated device address and to get the network and application session
     * keys.
     */
    puts("Starting join procedure");
    if (semtech_loramac_join(&loramac, LORAMAC_JOIN_OTAA) != SEMTECH_LORAMAC_JOIN_SUCCEEDED)
    {
        puts("Join procedure failed");
        return 1;
    }
    puts("Join procedure succeeded");
     printf("SCD30 mesurement:\n");
    setup_scd30();

    scd30_start_periodic_measurement(&scd30_dev, &interval,&pressure_compensation);
    time_t start_timer = 0;
    time_t end_timer = 0;
    time(&start_timer);
    get_mesurement();
    while (true) {
        time(&end_timer);
        if (end_timer-start_timer>5)
        {
            _send_message();
            time(&start_timer);
        }
        
    }

    scd30_stop_measurements(&scd30_dev);
    
    return 0;
}
