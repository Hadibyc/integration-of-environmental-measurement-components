#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#include "xtimer.h"
#include "timex.h"
#include "periph/gpio.h"

unsigned long duration;
time_t starttime;
time_t currenttime;
unsigned long sampletime_s = 30;
int pulse_time_out=5;
unsigned long lowpulseoccupancy = 0;
float ratio = 0;
float concentration = 0;
gpio_t pin;






unsigned long pulseIn(void){
  int high =0;
  time_t start_timer=0;
  time_t end_timer=0;
  time_t start=0;
  time_t end=0;
  time(&start_timer);
  while (high==0){
  
     
      if (gpio_read(pin)>0){
         
        	high=1;
      	 time(&start);
      
      	break ;
      	}
      	time(&end_timer);
      	if (end_timer-start_timer>pulse_time_out)
      		return 0;
      	
  }
  
  if (high==1){
  	
  	while (1){
  		if (gpio_read(pin)==0){
  		high=0;
  		
  		 time(&end);
  		
  		
  		break;
  		}
  		time(&end_timer);
  		if (end_timer-start_timer>pulse_time_out)
      		return 0;
  	}
  }
 unsigned long  elapsed = end - start;
 
  
  
  return elapsed;
  
  
 
} 

/* Function to calculate x raised to the power y */
int pow(int x, unsigned int y)
{
	if (y == 0)
		return 1;
	else if (y%2 == 0)
		return pow(x, y/2)*pow(x, y/2);
	else
		return x*pow(x, y/2)*pow(x, y/2);
}



void setup(void) 
{
    pin = GPIO_PIN(2,12);
    gpio_init(pin,GPIO_IN);
  
    
  
     time( &starttime );//get the current time;
}
 
void loop(void) 
{
    duration = pulseIn();
    duration=duration*1000000;
    lowpulseoccupancy = lowpulseoccupancy+duration;
    time( &currenttime );
    unsigned long elapsed = currenttime-starttime;
    printf("elapsed = %ld\r\n",elapsed);
    if (elapsed >= sampletime_s)//if the sampel time == 30s
    {
    	long int sampletime_ms = sampletime_s*1000;
        ratio = lowpulseoccupancy/(sampletime_ms*10.0);  // Integer percentage 0=>100
        concentration = 1.1*pow(ratio,3)-3.8*pow(ratio,2)+520*ratio+0.62; // using spec sheet curve
        printf("lowpulseoccupancy=%ld,ratio=%f,concentration=%f\r\n",lowpulseoccupancy,ratio,concentration);
     
        lowpulseoccupancy = 0;
        time(&starttime );
    } 
}

int main(void)
{
     
    
     setup();
    
   
    while (1) {
        loop();
    }

    return 0;
}