#include "arduino.hpp"
#include "board.h"

int pin = 8;
unsigned long duration;
unsigned long starttime;
unsigned long sampletime_ms = 10000;//sampe 30s ;
unsigned long lowpulseoccupancy = 0;
float ratio = 0;
float concentration = 0;


unsigned long pulseIn(int pin){
  int high =0;
  unsigned long start=0;
  unsigned long end=0;
  while (true){
      if (digitalRead(pin)!=0){
      	high=1;
      	start=millis();
      	break ;
      	}
  }
  if (high==1){
  	
  	while (true){
  		if (digitalRead(pin)==0){
  		high=0;
  		end=millis();
  		break;
  		}
  	}
  }
  return (end-start) ;
  
  
 
} 
#include<stdio.h>

/* Function to calculate x raised to the power y */
int pow(int x, unsigned int y)
{
	if (y == 0)
		return 1;
	else if (y%2 == 0)
		return pow(x, y/2)*pow(x, y/2);
	else
		return x*pow(x, y/2)*pow(x, y/2);
}



void setup() 
{
    Serial.begin(115200);
    pinMode(pin,INPUT);
    int a= gpio_is_valid (pin);
    Serial.print(a);
    
  
    starttime = millis();//get the current time;
}
 
void loop() 
{
    duration = pulseIn(pin);
    lowpulseoccupancy = lowpulseoccupancy+duration;
 
    if ((millis()-starttime) > sampletime_ms)//if the sampel time == 30s
    {
        ratio = lowpulseoccupancy/(sampletime_ms*10.0);  // Integer percentage 0=>100
        concentration = 1.1*pow(ratio,3)-3.8*pow(ratio,2)+520*ratio+0.62; // using spec sheet curve
        Serial.print(lowpulseoccupancy);
        Serial.print(",");
        Serial.print(ratio);
        Serial.print(",");
        Serial.println(concentration);
        lowpulseoccupancy = 0;
        starttime = millis();
    } 
}

int main(void)
{
    /* run the Arduino setup */
    setup();
    /* and the event loop */
    while (1) {
        loop();
    }
    /* never reached */
    return 0;
}
