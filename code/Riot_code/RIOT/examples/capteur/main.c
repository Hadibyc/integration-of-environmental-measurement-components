#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#include "xtimer.h"
#include "timex.h"
#include "periph/gpio.h"
int pin = 8;
unsigned long duration;
time_t starttime;
unsigned long sampletime_ms = 1000;
unsigned long lowpulseoccupancy = 0;
float ratio = 0;
float concentration = 0;



unsigned long pulseIn(int pin){
  int high =0;
  time_t start=0;
  time_t end=0;
  while (1){
      if (gpio_read(pin)!=0){
      	high=1;
      	 start=  time( NULL );
      	break ;
      	}
  }
  if (high==1){
  	
  	while (1){
  		if (gpio_read(pin)==0){
  		high=0;
  		end = time( NULL);
  		
  		break;
  		}
  	}
  }
  return (unsigned long) difftime( end, start ) ;
  
  
 
} 
#include<stdio.h>

/* Function to calculate x raised to the power y */
int pow(int x, unsigned int y)
{
	if (y == 0)
		return 1;
	else if (y%2 == 0)
		return pow(x, y/2)*pow(x, y/2);
	else
		return x*pow(x, y/2)*pow(x, y/2);
}



void setup(void) 
{
    gpio_init(pin,INPUT);
    int a= gpio_is_valid (pin);
    printf("%d",a);
    
  
    starttime =  time( NULL );//get the current time;
}
 
void loop(void) 
{
    duration = pulseIn(pin);
    lowpulseoccupancy = lowpulseoccupancy+duration;
 
    if (( time( NULL )-starttime) > sampletime_ms)//if the sampel time == 30s
    {
        ratio = lowpulseoccupancy/(sampletime_ms*10.0);  // Integer percentage 0=>100
        concentration = 1.1*pow(ratio,3)-3.8*pow(ratio,2)+520*ratio+0.62; // using spec sheet curve
        printf("%ld",lowpulseoccupancy);
        printf(",");
        printf("%f",ratio);
        printf(",");
        printf("%f",concentration);
        lowpulseoccupancy = 0;
        starttime =  time( NULL );
    } 
}

int main(void)
{
     puts("Test application ");
    /* run the Arduino setup */
    // setup();
    // /* and the event loop */
    // while (1) {
    //     loop();
    // }
    // /* never reached */
    return 0;
}
