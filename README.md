# Groupe 8 : Intégration de composants de mesures environnementales (eau, air...) pour le projet STM32Python à destination des lycéens

## Description du projet

Vous trouverez ici un descriptif du [projet STM32Python](https://air.imag.fr/index.php/Contribution_au_projet_STM32Python)

## Membres de l'équipe

* Mirette Guirguis
* Hachem Mohsen
* Chemsseddine Hadiby

## Sommaire

1. [Qu'est-ce que RIOT OS ?](#1-quest-ce-que-riot-os-?)
2. [Qu'est-ce que LoRaWAN ?](#2-quest-ce-que-lorawan-?)
3. [Qu'est-ce que Cayenne ?](#3-quest-ce-que-cayenne-?)
4. [Matériel utilisé](#4-matériel-utilisé)
5. [Fiche de suivi](#5-fiche-de-suivi)
*******

## 1. Qu'est-ce que RIOT OS ?

RIOT est un système d'exploitation léger pour systèmes en réseau avec des contraintes de mémoire, focalisé sur les appareils à faible consommation électrique pour l'Internet des objets. 

## 2. Qu'est-ce que LoRaWAN ?

LoRaWAN est un protocole réseau LPWAN (Low Power Wide Area network). Il s'appuie sur la technologie radio LoRa. Voici quelques informations sur la technologie LoRa et le protocole LoRaWAN :

- __Réseau sans fil__ basse consommation
- Convient pour les applications IoT (Internet of Things) ne nécessitant __pas de débit élevé__
- __Messages de très petite taille__
- __Longue portée__
- Objets avec une très __longue durée de vie des batteries__
- __Communication bidirectionnelle__

En synthèse, le protocole LoRaWAN permet la mise en oeuvre de moyens de collecte de données (objets connectés LoRaWAN) via un réseau sans fil bas débit.

## 3. Qu'est-ce que Cayenne ?

Cayenne est une plateforme IoT développée par MyDevice permettant aux sociétés de connecter des objets, de visualiser des données, d’appliquer facilement des règles sophistiquées et d’interagir avec leurs clients connectés. Cayenne est une solution gratuite, accessible en ligne ou via une application mobile.

## 4. Matériel utilisé

- __NUCLEO-WB55__ :

<div align="center">
<img alt="NUCLEO-WB55" src="images/NUCLEO-WB55.jpg" heigth="300px" width="300px">
</div>

    La carte NUCLEO-WB55 permet de programmer aisément le microcontrôleur STM32WB55 qui l’équipe en donnant accès à toutes ses fonctions.
    Les connecteurs d’extensions (Arduino) permettent à l’utilisateur de connecter des composants électroniques externes au STM32WB55.

<br>

- __Grove Base Shield__ :

<div align="center">
<img alt="Grove Base Shield" src="images/Grove_Base_Shield.jpg" heigth="300px" width="300px">
</div>

    Le module Grove Base Shield est une carte d'interface permettant de raccorder facilement, rapidement et sans soudure les capteurs et les actionneurs Grove de Seeedstudio sur une carte compatible Arduino.

<br>

- __LoRa-E5 Mini__ :

<div align="center">
<img alt="LoRa-E5 Mini" src="images/LoRa-E5_Mini.jpg" heigth="300px" width="300px">
</div>

    La carte LoRa-E5 Mini est une carte de développement compacte, adaptée aux petites applications nécessitant un processus de test rapide.

<br>

- __STM32 Discovery__ :

<div align="center">
<img alt="STM32 Discovery" src="images/STM32_Discovery.jpg" heigth="300px" width="300px">
</div>

    La famille STM32 est une série de microcontrôleurs 32-bits en circuits intégrés réalisés par la société Franco-Italienne STMicroelectronics.

<br>

- __Grove - Dust Sensor__ :

<div align="center">
<img alt="Grove-Dust Sensor" src="images/Grove-Dust_Sensor.jpeg" heigth="300px" width="300px">
</div>

    Ce module compatible Grove est conçu pour tester la qualité de l'air dans des pièces fermées. Les principaux gaz détectés sont le CO, l'alcool, l'acétone, le formaldéhyde et d'autres gaz légèrement toxiques. 

<br>

- __Grove - CO2 & Temperature & Humidity Sensor (SCD30)__ :

<div align="center">
<img alt="Grove - CO2 & Temperature & Humidity Sensor (SCD30)" src="images/Grove_-_CO2___Temperature___Humidity_Sensor__SCD30_.jpg" heigth="300px" width="300px">
</div>

    C'est un capteur Grove de CO2, de température et d'humidité de l'air ambiant pour réaliser des projets tels que des purificateurs d'air, des stations météos avec carte Arduino ou des systèmes de surveillance de l'environnement ou d'un bâtiment industriel.

## 5. Fiche de suivi

Vous trouverez ici une [fiche de suivi](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/21-22/08/docs/-/blob/main/Fiche%20de%20suivi.md)
